import { AddressService } from '../../../services/Address.service';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Address } from '../../../models/Address';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexAddressComponent implements OnInit {

  addresss: any;

  constructor(private http: HttpClient, 
  				private route: ActivatedRoute, 
  				private router: Router, 
  				private service: AddressService) {}

  ngOnInit() {
    this.getAddresss();
  }

  getAddresss() {
    this.service.getAddresss().subscribe(res => {
      this.addresss = res;
    });
  }

  deleteAddress(id) {
    this.service.deleteAddress(id)
		.then(success => 
			{
				this.router.navigateByUrl('/', {skipLocationChange: false}).then(()=>
							this.router.navigate(['indexAddress']));
			});  }
}
