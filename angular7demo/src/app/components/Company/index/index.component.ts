import { CompanyService } from '../../../services/Company.service';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Company } from '../../../models/Company';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexCompanyComponent implements OnInit {

  companys: any;

  constructor(private http: HttpClient, 
  				private route: ActivatedRoute, 
  				private router: Router, 
  				private service: CompanyService) {}

  ngOnInit() {
    this.getCompanys();
  }

  getCompanys() {
    this.service.getCompanys().subscribe(res => {
      this.companys = res;
    });
  }

  deleteCompany(id) {
    this.service.deleteCompany(id)
		.then(success => 
			{
				this.router.navigateByUrl('/', {skipLocationChange: false}).then(()=>
							this.router.navigate(['indexCompany']));
			});  }
}
