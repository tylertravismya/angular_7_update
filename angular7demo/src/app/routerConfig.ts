// routerConfig.ts

import { Routes } from '@angular/router';
import { CreateAddressComponent } from './components/Address/create/create.component';
import { EditAddressComponent } from './components/Address/edit/edit.component';
import { IndexAddressComponent } from './components/Address/index/index.component';
import { CreateCompanyComponent } from './components/Company/create/create.component';
import { EditCompanyComponent } from './components/Company/edit/edit.component';
import { IndexCompanyComponent } from './components/Company/index/index.component';
import { CreateDepartmentComponent } from './components/Department/create/create.component';
import { EditDepartmentComponent } from './components/Department/edit/edit.component';
import { IndexDepartmentComponent } from './components/Department/index/index.component';
import { CreateDivisionComponent } from './components/Division/create/create.component';
import { EditDivisionComponent } from './components/Division/edit/edit.component';
import { IndexDivisionComponent } from './components/Division/index/index.component';
import { CreateEmployeeComponent } from './components/Employee/create/create.component';
import { EditEmployeeComponent } from './components/Employee/edit/edit.component';
import { IndexEmployeeComponent } from './components/Employee/index/index.component';

export const AddressRoutes: Routes = [
  { path: 'createAddress',
    component: CreateAddressComponent
  },
  {
    path: 'editAddress/:id',
    component: EditAddressComponent
  },
  { path: 'indexAddress',
    component: IndexAddressComponent
  }
];
export const CompanyRoutes: Routes = [
  { path: 'createCompany',
    component: CreateCompanyComponent
  },
  {
    path: 'editCompany/:id',
    component: EditCompanyComponent
  },
  { path: 'indexCompany',
    component: IndexCompanyComponent
  }
];
export const DepartmentRoutes: Routes = [
  { path: 'createDepartment',
    component: CreateDepartmentComponent
  },
  {
    path: 'editDepartment/:id',
    component: EditDepartmentComponent
  },
  { path: 'indexDepartment',
    component: IndexDepartmentComponent
  }
];
export const DivisionRoutes: Routes = [
  { path: 'createDivision',
    component: CreateDivisionComponent
  },
  {
    path: 'editDivision/:id',
    component: EditDivisionComponent
  },
  { path: 'indexDivision',
    component: IndexDivisionComponent
  }
];
export const EmployeeRoutes: Routes = [
  { path: 'createEmployee',
    component: CreateEmployeeComponent
  },
  {
    path: 'editEmployee/:id',
    component: EditEmployeeComponent
  },
  { path: 'indexEmployee',
    component: IndexEmployeeComponent
  }
];
