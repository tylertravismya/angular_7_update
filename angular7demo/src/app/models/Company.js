var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Define collection and schema for Company
var Company = new Schema({
  name: {
	type : String
  },
  establishedOn: {
	type : String
  },
  revenue: {
	type : String
  },
  Employees: {
 	type : [{ type: Schema.Types.ObjectId, ref: 'Employee' }]
  },
  Departments: {
 	type : [{ type: Schema.Types.ObjectId, ref: 'Department' }]
  },
  Divisions: {
 	type : [{ type: Schema.Types.ObjectId, ref: 'Division' }]
  },
  BoardMembers: {
 	type : [{ type: Schema.Types.ObjectId, ref: 'Employee' }]
  },
  Address: {
	type : Schema.Types.ObjectId
  },
  Type: {
 	type : String
  },
  Industry: {
 	type : String
  },
},{
    collection: 'companys'
});

module.exports = mongoose.model('Company', Company);