import { Injectable } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import {Employee} from '../models/Employee';
import { HelperBaseService } from './helperbase.service';

@Injectable({
	providedIn: 'root'
  })
    
export class EmployeeService extends HelperBaseService {

	//********************************************************************
	// general holder 
	//********************************************************************
	employee : any;
	
	//********************************************************************
	// Catch all for the return value of a service call
	//********************************************************************
	result: any;

	//********************************************************************
	// sole constructor, injected with the HttpClient
	//********************************************************************
 	constructor(private http: HttpClient) {
 	    super();
    }
 	
	//********************************************************************
	// add a Employee 
	// returns the results untouched as a JSON representation 
	// delegates via URI to an ORM handler
	//********************************************************************
  	addEmployee(firstName, lastName, Type) : Promise<any> {
    	const uri = this.ormUrl + '/Employee/add';
    	const obj = {
      		firstName: firstName,
      		lastName: lastName,
			Type: Type
    	};
    	
    	return this.http.post(uri, obj).toPromise();
  	}

	//********************************************************************
	// gets all Employee 
	// returns the results untouched as JSON representation of an
	// array of Employee models
	// delegates via URI to an ORM handler
	//********************************************************************
	getEmployees() {
    	const uri = this.ormUrl + '/Employee';
    	
    	return this
            	.http.get(uri).map(res => {
              						return res;
            					});
  	}

	//********************************************************************
	// edit a Employee 
	// returns the results untouched as a JSON representation of a
	// Employee model
	// delegates via URI to an ORM handler
	//********************************************************************
  	editEmployee(id) {
    	const uri = this.ormUrl + '/Employee/edit/' + id;
    	
    	return this.http.get(uri).map(res => {
              							return res;
            						});
  	}

	//********************************************************************
	// update a Employee 
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	updateEmployee(firstName, lastName, Type, id)  : Promise<any>  {
    	const uri = this.ormUrl + '/Employee/update/' + id;
    	const obj = {
      		firstName: firstName,
      		lastName: lastName,
			Type: Type
    	};
    	
    	return this.http.post(uri, obj).toPromise();
  	}

	//********************************************************************
	// delete a Employee 
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	deleteEmployee(id)  : Promise<any> {
    	const uri = this.ormUrl + '/Employee/delete/' + id;

        return this.http.get(uri).toPromise();
  }
  
    	

	//********************************************************************
	// saveHelper - internal helper to save a Employee
	//********************************************************************
	saveHelper() : Promise<any> {
		
		const uri = this.ormUrl + '/Employee/update/' + this.employee._id;		
		
    	return this
      			.http
      			.post(uri, this.employee)
				.toPromise();			
	}

	//********************************************************************
	// loadHelper - internal helper to load a Employee
	//********************************************************************	
	loadHelper( id ) {
		this.editEmployee(id)
        		.subscribe(res => {
        			this.employee = res;
      			});
	}
}