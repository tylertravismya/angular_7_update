import { Injectable } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import {Address} from '../models/Address';
import { HelperBaseService } from './helperbase.service';

@Injectable({
	providedIn: 'root'
  })
    
export class AddressService extends HelperBaseService {

	//********************************************************************
	// general holder 
	//********************************************************************
	address : any;
	
	//********************************************************************
	// Catch all for the return value of a service call
	//********************************************************************
	result: any;

	//********************************************************************
	// sole constructor, injected with the HttpClient
	//********************************************************************
 	constructor(private http: HttpClient) {
 	    super();
    }
 	
	//********************************************************************
	// add a Address 
	// returns the results untouched as a JSON representation 
	// delegates via URI to an ORM handler
	//********************************************************************
  	addAddress(street, city, state, zipCode) : Promise<any> {
    	const uri = this.ormUrl + '/Address/add';
    	const obj = {
      		street: street,
      		city: city,
      		state: state,
			zipCode: zipCode
    	};
    	
    	return this.http.post(uri, obj).toPromise();
  	}

	//********************************************************************
	// gets all Address 
	// returns the results untouched as JSON representation of an
	// array of Address models
	// delegates via URI to an ORM handler
	//********************************************************************
	getAddresss() {
    	const uri = this.ormUrl + '/Address';
    	
    	return this
            	.http.get(uri).map(res => {
              						return res;
            					});
  	}

	//********************************************************************
	// edit a Address 
	// returns the results untouched as a JSON representation of a
	// Address model
	// delegates via URI to an ORM handler
	//********************************************************************
  	editAddress(id) {
    	const uri = this.ormUrl + '/Address/edit/' + id;
    	
    	return this.http.get(uri).map(res => {
              							return res;
            						});
  	}

	//********************************************************************
	// update a Address 
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	updateAddress(street, city, state, zipCode, id)  : Promise<any>  {
    	const uri = this.ormUrl + '/Address/update/' + id;
    	const obj = {
      		street: street,
      		city: city,
      		state: state,
			zipCode: zipCode
    	};
    	
    	return this.http.post(uri, obj).toPromise();
  	}

	//********************************************************************
	// delete a Address 
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	deleteAddress(id)  : Promise<any> {
    	const uri = this.ormUrl + '/Address/delete/' + id;

        return this.http.get(uri).toPromise();
  }
  
    	

	//********************************************************************
	// saveHelper - internal helper to save a Address
	//********************************************************************
	saveHelper() : Promise<any> {
		
		const uri = this.ormUrl + '/Address/update/' + this.address._id;		
		
    	return this
      			.http
      			.post(uri, this.address)
				.toPromise();			
	}

	//********************************************************************
	// loadHelper - internal helper to load a Address
	//********************************************************************	
	loadHelper( id ) {
		this.editAddress(id)
        		.subscribe(res => {
        			this.address = res;
      			});
	}
}