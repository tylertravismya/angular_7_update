import { Injectable } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import {Company} from '../models/Company';
import {EmployeeService} from '../services/Employee.service';
import {DepartmentService} from '../services/Department.service';
import {DivisionService} from '../services/Division.service';
import {AddressService} from '../services/Address.service';
import { HelperBaseService } from './helperbase.service';

@Injectable({
	providedIn: 'root'
  })
    
export class CompanyService extends HelperBaseService {

	//********************************************************************
	// general holder 
	//********************************************************************
	company : any;
	
	//********************************************************************
	// Catch all for the return value of a service call
	//********************************************************************
	result: any;

	//********************************************************************
	// sole constructor, injected with the HttpClient
	//********************************************************************
 	constructor(private http: HttpClient) {
 	    super();
    }
 	
	//********************************************************************
	// add a Company 
	// returns the results untouched as a JSON representation 
	// delegates via URI to an ORM handler
	//********************************************************************
  	addCompany(name, establishedOn, revenue, Employees, Departments, Divisions, BoardMembers, Address, Type, Industry) : Promise<any> {
    	const uri = this.ormUrl + '/Company/add';
    	const obj = {
      		name: name,
      		establishedOn: establishedOn,
      		revenue: revenue,
      		Employees: Employees != null && Employees.length > 0 ? Employees : null,
      		Departments: Departments != null && Departments.length > 0 ? Departments : null,
      		Divisions: Divisions != null && Divisions.length > 0 ? Divisions : null,
      		BoardMembers: BoardMembers != null && BoardMembers.length > 0 ? BoardMembers : null,
      		Address: Address != null && Address.length > 0 ? Address : null,
      		Type: Type,
			Industry: Industry
    	};
    	
    	return this.http.post(uri, obj).toPromise();
  	}

	//********************************************************************
	// gets all Company 
	// returns the results untouched as JSON representation of an
	// array of Company models
	// delegates via URI to an ORM handler
	//********************************************************************
	getCompanys() {
    	const uri = this.ormUrl + '/Company';
    	
    	return this
            	.http.get(uri).map(res => {
              						return res;
            					});
  	}

	//********************************************************************
	// edit a Company 
	// returns the results untouched as a JSON representation of a
	// Company model
	// delegates via URI to an ORM handler
	//********************************************************************
  	editCompany(id) {
    	const uri = this.ormUrl + '/Company/edit/' + id;
    	
    	return this.http.get(uri).map(res => {
              							return res;
            						});
  	}

	//********************************************************************
	// update a Company 
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	updateCompany(name, establishedOn, revenue, Employees, Departments, Divisions, BoardMembers, Address, Type, Industry, id)  : Promise<any>  {
    	const uri = this.ormUrl + '/Company/update/' + id;
    	const obj = {
      		name: name,
      		establishedOn: establishedOn,
      		revenue: revenue,
      		Employees: Employees != null && Employees.length > 0 ? Employees : null,
      		Departments: Departments != null && Departments.length > 0 ? Departments : null,
      		Divisions: Divisions != null && Divisions.length > 0 ? Divisions : null,
      		BoardMembers: BoardMembers != null && BoardMembers.length > 0 ? BoardMembers : null,
      		Address: Address != null && Address.length > 0 ? Address : null,
      		Type: Type,
			Industry: Industry
    	};
    	
    	return this.http.post(uri, obj).toPromise();
  	}

	//********************************************************************
	// delete a Company 
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	deleteCompany(id)  : Promise<any> {
    	const uri = this.ormUrl + '/Company/delete/' + id;

        return this.http.get(uri).toPromise();
  }
  
    		//********************************************************************
	// assigns a Address on a Company
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	assignAddress( companyId, _addressId ): Promise<any> {

		// get the Company from storage
		this.loadHelper( companyId );
		
		// get the Address from storage
		var tmp 	= new AddressService(this.http).editAddress(_addressId);
		
		// assign the Address		
		this.company.address = tmp;
      		
		// save the Company
		return this.saveHelper();		
	}

	//********************************************************************
	// unassigns a Address on a Company
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************				
	unassignAddress( companyId ): Promise<any> {

		// get the Company from storage
        this.loadHelper( companyId );
		
		// assign Address to null		
		this.company.address = null;
      		
		// save the Company
		return this.saveHelper();
	}
	

	//********************************************************************
	// adds one or more employeesIds as a Employees 
	// to a Company
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************				
	addEmployees( companyId, employeesIds ): Promise<any> {

		// get the Company
		this.loadHelper( companyId );
				
		// split on a comma with no spaces
		var idList = employeesIds.split(',')

		// iterate over array of employees ids
		idList.forEach(function (id) {
			// read the Employee		
			var employee = new EmployeeService(this.http).editEmployee(id);	
			// add the Employee if not already assigned
			if ( this.company.employees.indexOf(employee) == -1 )
				this.company.employees.push(employee);
		});
				
		// save it		
		return this.saveHelper();
	}			
	
	//********************************************************************
	// removes one or more employeesIds as a Employees 
	// from a Company
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************						
	removeEmployees( companyId, employeesIds ): Promise<any> {
		
		// get the Company
		this.loadHelper( companyId );

				
		// split on a comma with no spaces
		var idList 					= employeesIds.split(',');
		var employees 	= this.company.employees;
		
		if ( employees != null && employeesIds != null ) {
		
			// iterate over array of employees ids
			employees.forEach(function (obj) {				
				if ( employeesIds.indexOf(obj._id) > -1 ) {
					 // remove the Employee
					this.company.employees.pop(obj);
				}
			});
					
		    // save it		
			return this.saveHelper();
		}
	}
			
	//********************************************************************
	// adds one or more departmentsIds as a Departments 
	// to a Company
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************				
	addDepartments( companyId, departmentsIds ): Promise<any> {

		// get the Company
		this.loadHelper( companyId );
				
		// split on a comma with no spaces
		var idList = departmentsIds.split(',')

		// iterate over array of departments ids
		idList.forEach(function (id) {
			// read the Department		
			var department = new DepartmentService(this.http).editDepartment(id);	
			// add the Department if not already assigned
			if ( this.company.departments.indexOf(department) == -1 )
				this.company.departments.push(department);
		});
				
		// save it		
		return this.saveHelper();
	}			
	
	//********************************************************************
	// removes one or more departmentsIds as a Departments 
	// from a Company
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************						
	removeDepartments( companyId, departmentsIds ): Promise<any> {
		
		// get the Company
		this.loadHelper( companyId );

				
		// split on a comma with no spaces
		var idList 					= departmentsIds.split(',');
		var departments 	= this.company.departments;
		
		if ( departments != null && departmentsIds != null ) {
		
			// iterate over array of departments ids
			departments.forEach(function (obj) {				
				if ( departmentsIds.indexOf(obj._id) > -1 ) {
					 // remove the Department
					this.company.departments.pop(obj);
				}
			});
					
		    // save it		
			return this.saveHelper();
		}
	}
			
	//********************************************************************
	// adds one or more divisionsIds as a Divisions 
	// to a Company
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************				
	addDivisions( companyId, divisionsIds ): Promise<any> {

		// get the Company
		this.loadHelper( companyId );
				
		// split on a comma with no spaces
		var idList = divisionsIds.split(',')

		// iterate over array of divisions ids
		idList.forEach(function (id) {
			// read the Division		
			var division = new DivisionService(this.http).editDivision(id);	
			// add the Division if not already assigned
			if ( this.company.divisions.indexOf(division) == -1 )
				this.company.divisions.push(division);
		});
				
		// save it		
		return this.saveHelper();
	}			
	
	//********************************************************************
	// removes one or more divisionsIds as a Divisions 
	// from a Company
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************						
	removeDivisions( companyId, divisionsIds ): Promise<any> {
		
		// get the Company
		this.loadHelper( companyId );

				
		// split on a comma with no spaces
		var idList 					= divisionsIds.split(',');
		var divisions 	= this.company.divisions;
		
		if ( divisions != null && divisionsIds != null ) {
		
			// iterate over array of divisions ids
			divisions.forEach(function (obj) {				
				if ( divisionsIds.indexOf(obj._id) > -1 ) {
					 // remove the Division
					this.company.divisions.pop(obj);
				}
			});
					
		    // save it		
			return this.saveHelper();
		}
	}
			
	//********************************************************************
	// adds one or more boardMembersIds as a BoardMembers 
	// to a Company
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************				
	addBoardMembers( companyId, boardMembersIds ): Promise<any> {

		// get the Company
		this.loadHelper( companyId );
				
		// split on a comma with no spaces
		var idList = boardMembersIds.split(',')

		// iterate over array of boardMembers ids
		idList.forEach(function (id) {
			// read the Employee		
			var employee = new EmployeeService(this.http).editEmployee(id);	
			// add the Employee if not already assigned
			if ( this.company.boardMembers.indexOf(employee) == -1 )
				this.company.boardMembers.push(employee);
		});
				
		// save it		
		return this.saveHelper();
	}			
	
	//********************************************************************
	// removes one or more boardMembersIds as a BoardMembers 
	// from a Company
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************						
	removeBoardMembers( companyId, boardMembersIds ): Promise<any> {
		
		// get the Company
		this.loadHelper( companyId );

				
		// split on a comma with no spaces
		var idList 					= boardMembersIds.split(',');
		var boardMembers 	= this.company.boardMembers;
		
		if ( boardMembers != null && boardMembersIds != null ) {
		
			// iterate over array of boardMembers ids
			boardMembers.forEach(function (obj) {				
				if ( boardMembersIds.indexOf(obj._id) > -1 ) {
					 // remove the Employee
					this.company.boardMembers.pop(obj);
				}
			});
					
		    // save it		
			return this.saveHelper();
		}
	}
			

	//********************************************************************
	// saveHelper - internal helper to save a Company
	//********************************************************************
	saveHelper() : Promise<any> {
		
		const uri = this.ormUrl + '/Company/update/' + this.company._id;		
		
    	return this
      			.http
      			.post(uri, this.company)
				.toPromise();			
	}

	//********************************************************************
	// loadHelper - internal helper to load a Company
	//********************************************************************	
	loadHelper( id ) {
		this.editCompany(id)
        		.subscribe(res => {
        			this.company = res;
      			});
	}
}