import { Injectable } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import {Division} from '../models/Division';
import {EmployeeService} from '../services/Employee.service';
import { HelperBaseService } from './helperbase.service';

@Injectable({
	providedIn: 'root'
  })
    
export class DivisionService extends HelperBaseService {

	//********************************************************************
	// general holder 
	//********************************************************************
	division : any;
	
	//********************************************************************
	// Catch all for the return value of a service call
	//********************************************************************
	result: any;

	//********************************************************************
	// sole constructor, injected with the HttpClient
	//********************************************************************
 	constructor(private http: HttpClient) {
 	    super();
    }
 	
	//********************************************************************
	// add a Division 
	// returns the results untouched as a JSON representation 
	// delegates via URI to an ORM handler
	//********************************************************************
  	addDivision(name, Head) : Promise<any> {
    	const uri = this.ormUrl + '/Division/add';
    	const obj = {
      		name: name,
			Head: Head != null && Head.length > 0 ? Head : null
    	};
    	
    	return this.http.post(uri, obj).toPromise();
  	}

	//********************************************************************
	// gets all Division 
	// returns the results untouched as JSON representation of an
	// array of Division models
	// delegates via URI to an ORM handler
	//********************************************************************
	getDivisions() {
    	const uri = this.ormUrl + '/Division';
    	
    	return this
            	.http.get(uri).map(res => {
              						return res;
            					});
  	}

	//********************************************************************
	// edit a Division 
	// returns the results untouched as a JSON representation of a
	// Division model
	// delegates via URI to an ORM handler
	//********************************************************************
  	editDivision(id) {
    	const uri = this.ormUrl + '/Division/edit/' + id;
    	
    	return this.http.get(uri).map(res => {
              							return res;
            						});
  	}

	//********************************************************************
	// update a Division 
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	updateDivision(name, Head, id)  : Promise<any>  {
    	const uri = this.ormUrl + '/Division/update/' + id;
    	const obj = {
      		name: name,
			Head: Head != null && Head.length > 0 ? Head : null
    	};
    	
    	return this.http.post(uri, obj).toPromise();
  	}

	//********************************************************************
	// delete a Division 
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	deleteDivision(id)  : Promise<any> {
    	const uri = this.ormUrl + '/Division/delete/' + id;

        return this.http.get(uri).toPromise();
  }
  
    		//********************************************************************
	// assigns a Head on a Division
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	assignHead( divisionId, _headId ): Promise<any> {

		// get the Division from storage
		this.loadHelper( divisionId );
		
		// get the Employee from storage
		var tmp 	= new EmployeeService(this.http).editEmployee(_headId);
		
		// assign the Head		
		this.division.head = tmp;
      		
		// save the Division
		return this.saveHelper();		
	}

	//********************************************************************
	// unassigns a Head on a Division
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************				
	unassignHead( divisionId ): Promise<any> {

		// get the Division from storage
        this.loadHelper( divisionId );
		
		// assign Head to null		
		this.division.head = null;
      		
		// save the Division
		return this.saveHelper();
	}
	


	//********************************************************************
	// saveHelper - internal helper to save a Division
	//********************************************************************
	saveHelper() : Promise<any> {
		
		const uri = this.ormUrl + '/Division/update/' + this.division._id;		
		
    	return this
      			.http
      			.post(uri, this.division)
				.toPromise();			
	}

	//********************************************************************
	// loadHelper - internal helper to load a Division
	//********************************************************************	
	loadHelper( id ) {
		this.editDivision(id)
        		.subscribe(res => {
        			this.division = res;
      			});
	}
}