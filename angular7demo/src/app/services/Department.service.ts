import { Injectable } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import {Department} from '../models/Department';
import {EmployeeService} from '../services/Employee.service';
import { HelperBaseService } from './helperbase.service';

@Injectable({
	providedIn: 'root'
  })
    
export class DepartmentService extends HelperBaseService {

	//********************************************************************
	// general holder 
	//********************************************************************
	department : any;
	
	//********************************************************************
	// Catch all for the return value of a service call
	//********************************************************************
	result: any;

	//********************************************************************
	// sole constructor, injected with the HttpClient
	//********************************************************************
 	constructor(private http: HttpClient) {
 	    super();
    }
 	
	//********************************************************************
	// add a Department 
	// returns the results untouched as a JSON representation 
	// delegates via URI to an ORM handler
	//********************************************************************
  	addDepartment(name, Head) : Promise<any> {
    	const uri = this.ormUrl + '/Department/add';
    	const obj = {
      		name: name,
			Head: Head != null && Head.length > 0 ? Head : null
    	};
    	
    	return this.http.post(uri, obj).toPromise();
  	}

	//********************************************************************
	// gets all Department 
	// returns the results untouched as JSON representation of an
	// array of Department models
	// delegates via URI to an ORM handler
	//********************************************************************
	getDepartments() {
    	const uri = this.ormUrl + '/Department';
    	
    	return this
            	.http.get(uri).map(res => {
              						return res;
            					});
  	}

	//********************************************************************
	// edit a Department 
	// returns the results untouched as a JSON representation of a
	// Department model
	// delegates via URI to an ORM handler
	//********************************************************************
  	editDepartment(id) {
    	const uri = this.ormUrl + '/Department/edit/' + id;
    	
    	return this.http.get(uri).map(res => {
              							return res;
            						});
  	}

	//********************************************************************
	// update a Department 
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	updateDepartment(name, Head, id)  : Promise<any>  {
    	const uri = this.ormUrl + '/Department/update/' + id;
    	const obj = {
      		name: name,
			Head: Head != null && Head.length > 0 ? Head : null
    	};
    	
    	return this.http.post(uri, obj).toPromise();
  	}

	//********************************************************************
	// delete a Department 
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	deleteDepartment(id)  : Promise<any> {
    	const uri = this.ormUrl + '/Department/delete/' + id;

        return this.http.get(uri).toPromise();
  }
  
    		//********************************************************************
	// assigns a Head on a Department
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	assignHead( departmentId, _headId ): Promise<any> {

		// get the Department from storage
		this.loadHelper( departmentId );
		
		// get the Employee from storage
		var tmp 	= new EmployeeService(this.http).editEmployee(_headId);
		
		// assign the Head		
		this.department.head = tmp;
      		
		// save the Department
		return this.saveHelper();		
	}

	//********************************************************************
	// unassigns a Head on a Department
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************				
	unassignHead( departmentId ): Promise<any> {

		// get the Department from storage
        this.loadHelper( departmentId );
		
		// assign Head to null		
		this.department.head = null;
      		
		// save the Department
		return this.saveHelper();
	}
	


	//********************************************************************
	// saveHelper - internal helper to save a Department
	//********************************************************************
	saveHelper() : Promise<any> {
		
		const uri = this.ormUrl + '/Department/update/' + this.department._id;		
		
    	return this
      			.http
      			.post(uri, this.department)
				.toPromise();			
	}

	//********************************************************************
	// loadHelper - internal helper to load a Department
	//********************************************************************	
	loadHelper( id ) {
		this.editDepartment(id)
        		.subscribe(res => {
        			this.department = res;
      			});
	}
}